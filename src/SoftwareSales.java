
public class SoftwareSales { 
	
	public double calculatePrice(int quantity) {
		
		if(quantity >= 10 && quantity <= 19) {
			return quantity*99 - 0.20*quantity*99;
		}
		else if(quantity >= 20 && quantity <= 49) {
			return quantity*99 - 0.30*quantity*99;
		}
		
		else
		return quantity*99;
	}

}
