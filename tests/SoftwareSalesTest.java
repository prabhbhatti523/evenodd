import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SoftwareSalesTest {

	SoftwareSales s1;
	@Before
	public void setUp() throws Exception {
		s1 = new SoftwareSales();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSoftwareSales1() {
		
		assertEquals(99, s1.calculatePrice(1), 0.01);
	}
	//R2:  Discount for 10-19 packages is 20%
	@Test
	public void testS10To19() {
		
		assertEquals(950.40, s1.calculatePrice(12), 0.01); 
	}
	//R3:  Discount for 20-49 packages is 30%
	@Test
	public void testS20To49() {
		
		assertEquals(2079, s1.calculatePrice(30), 0.01);
	}
}
